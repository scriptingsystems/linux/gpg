# Situación

El **Usuario A** haciendo uso de la tecnologia GPG generar un par de claves publica y privada, la clave publica la compartira con el **Usuario B**, para que este pueda firmar ficheros con esa clave publica, y el **Usuario A** pueda descrifrarlo


## Usuario A

1. Generar las claves publicas
```bash
gpg --gen-key
```bash

2. Listas las llaves publicas que se han creado
```bash
gpg --list-keys o -k

[root@ansible-s2 TestingGPG]# gpg --list-keys
/root/.gnupg/pubring.kbx
------------------------
pub   rsa2048 2023-05-28 [SC] [caduca: 2025-05-27]
      9C06F675B2C48B34C7DB49D09D8D6CF45C8E6596
uid        [  absoluta ] Mr. Encrypt <jeancarlos.rojas@icloud.com>
sub   rsa2048 2023-05-28 [E] [caduca: 2025-05-27]
```

3. Exportar las claves, para compartirlo con el **Usuario B**
```bash
gpg --output bestuser-gpg.pub --export 9C06F675B2C48B34C7DB49D09D8D6CF45C8E6596
```

Compartimos la clave publica con el **Usuario B** para que cifre los ficheros


## Usuario B

1. Compartido la clave publica con este usuario, desde la sesion de ese usuario, se tendra que importar esa clave publica
```bash
gpg --import bestuser-gpg.pub
```

2. Verificamos que ya tenemos incluida nuestra clave publica en ese usuario
```bash
[automation@ansible-s2 ~]$ gpg --list-keys
/home/automation/.gnupg/pubring.kbx
-----------------------------------
pub   rsa2048 2023-05-28 [SC] [expires: 2025-05-27]
      9C06F675B2C48B34C7DB49D09D8D6CF45C8E6596
uid           [ unknown] Mr. Encrypt <jeancarlos.rojas@icloud.com>
sub   rsa2048 2023-05-28 [E] [expires: 2025-05-27]
```

3. Creamos un fichero de nombre **mySecretAutomation.txt**
```bash
$ echo "This is my file, my hostname is ${HOSTNAME}" > mySecretAutomation.txt
```

4. Cifrar ese fichero creado con esa clave publica
```bash
gpg -r 9C06F675B2C48B34C7DB49D09D8D6CF45C8E6596 -e mySecretAutomation.txt
```

**Nota:** Recuerda que el destinatario debe tener la clave privada correspondiente para descifrar el archivo. Además, es importante tener en cuenta las consideraciones de seguridad al compartir archivos cifrados

Esto generar el siguiente fichero, que sera compartido al **usuario A** para que lo descrifre **mySecretAutomation.txt.gpg**


## Usuario A

1. Para descrifrar el archivo, puedes utilizar el siguiente comando:
```bash
gpg -d /tmp/mySecretAutomation.txt.gpg
```

El comando solicitará automáticamente la clave privada correspondiente para descifrar el archivo.

Recuerda que para descifrar el archivo, debes tener acceso a la clave privada correspondiente al par de claves utilizadas para cifrarlo. Asegúrate de que la clave privada esté protegida y sea accesible solo para las personas autorizadas.
